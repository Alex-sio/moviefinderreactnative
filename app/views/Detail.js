import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';

export const DetailPage = ({navigation, route}) => {

    //liste des constantes pour la requete de l'API
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);

    //fonction permettant de récupérer les détails d'un film avec l'iD de l'item provenant de la liste
    useEffect(() => {
        fetch('http://www.omdbapi.com/?i='+ route.params.item +'&apikey=68a85415')
          .then((response) => response.json())
          .then((responseJSON) => setData(responseJSON))
          .catch((error) => console.error(error))
          .finally(() => setLoading(false));
      }, []);

    //affiche les détail du film
    return (
        //conteneur principale de la page
        <View style={styles.container}>
            {/* Conteneur du titre du film */}
            <View style={styles.headStyle}>
                <Text style={styles.headText}>{data.Title}</Text>
            </View>

            {/* conteneur du poster et de la description du film mis a la ligne */}
            <View style={styles.row}>
                {/* le poster */}
                <Image style={styles.Image} source={{uri: data.Poster}} />
                {/* conteneur avec la description, l'année et la durée du film */}
                <View style={styles.plotBox}>
                    <View style={styles.row}>
                        <Text style={styles.topText}>{data.Year}</Text>
                        <Text style={styles.topText}>{data.Runtime}</Text>
                    </View>
                    <Text style={styles.plot}>{data.Plot}</Text>
                </View>
            </View>

            {/* conteneur contenant le genre, le pays, le directeur et les acteurs du film */}
            <View>

                <View style={styles.line}>
                    <Text style={styles.title}>Genre</Text>
                    <Text style={styles.text}>{data.Genre}</Text>
                </View>

                <View style={styles.line}>
                    <Text style={styles.title}>Pays</Text>
                    <Text style={styles.text}>{data.Country}</Text>
                </View>

                <View style={styles.line}>
                    <Text style={styles.title}>Directeur</Text>
                    <Text style={styles.text}>{data.Director}</Text>
                </View>

                <View style={styles.line}>
                    <Text style={styles.title}>Acteurs</Text>
                    <Text style={styles.text}>{data.Actors}</Text>
                </View>
            </View>
        </View>
    )
}

//styles de la pages
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        backgroundColor: '#4D4A95',
        paddingTop: 20,
    },
    headText: {
        color: '#4D4A95',
        fontSize: 20,
        flex: 4,
        textAlign: 'center',
        fontWeight: '700'
    },
    headStyle: {
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        backgroundColor: '#fff',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        maxHeight: 80,
        borderRadius: 10,
    },
    Image:{
        width: 150,
        height: 300,
        borderRadius: 10,
        marginLeft: 10,
        marginRight: 20,
    },
    row:{
        marginTop: 30,
        flex: 1,
        flexDirection: 'row',
    },
    topText:{
        marginRight: 30,
        color: '#fff',
        fontWeight: '700',
    },
    plotBox:{
        width: 250,
        height: 300,
    },
    plot:{
        color: '#fff',
        width: 180,
        height: 230,
    },
    text:{
        color: '#fff',
        margin: 2,
        paddingLeft: 10,
        paddingTop: 2,
        paddingBottom: 2,
    },
    title:{
        color: '#4D4A95',
        backgroundColor: '#fff',
        width: 80,
        height: 20,
        textAlign: 'center',
        borderRadius: 10,
        fontWeight: '700'
    },
    line:{
        alignItems: 'center',
        flexDirection: 'row',
        width: '100%',
        height: 50,
    }
});