import React, {useState} from 'react';
import * as Location from 'expo-location';
import { Text, TextInput, View, StyleSheet, Pressable, Button} from 'react-native';

export const HomePage = ({navigation, route}) => {

    //liste des constantes pour la géolocalisation
    const [texte, setTexte] = useState('');
    const [location, setLocation] = useState(null);
    const [errorMsg, setErrorMsg] = useState(null);

    React.useEffect(() => {
        (async () => {
            //demande la permission d'utiliser la géolocalisation
            let { status } = await Location.requestForegroundPermissionsAsync();
            if (status !== 'granted') {
                //si la géolocalisation n'est pas permise, alors on ajoute un message d'erreur
                //dans la constante "errorMsg"
                setErrorMsg('Permission to access location was denied');
                return;
        }
    
        //si la géolocalisation est permise, on recupere la localisation du téléphone
        //puis on les ajoutes dans la constant "location"
        let { coords } = await Location.getCurrentPositionAsync({});
        const { latitude, longitude } = coords;
        let town = await Location.reverseGeocodeAsync({latitude, longitude});
        setLocation(town);
        })();
    }, []);
    
    //le temps du chargement, on met un text d'attente dans la constante "text"
    let text = 'Waiting..';
    
    if (errorMsg) {
        //si un message d'erreur existe dans la constante "errorMsg"
        //on affiche le message d'error
        text = errorMsg;
    } else if (location) {
        text = location[0].country;
    }

    return (
        //conteneur principale
        <View style = {styles.container}>
            {/* Titre de l'application */}
            <Text style = {styles.title}>Movie Finder</Text>
            {/* Conteneur de recherche */}
            <View style={styles.search} >
                <TextInput style = {styles.input}
                placeholder="Rechercher un film"
                onChangeText={(newText) => setTexte(newText)}/>
                {/* Bouton qui envoie la saisie dans la requete de l'api */}
                {/* qui se situe sur la page ListPage */}
                <Pressable style={styles.btn_search} 
                onPress={() =>
                    navigation.navigate('ListPage', { name: texte })
                }
                >
                    <Text style={{color: '#fff'}}>Rechercher</Text>
                </Pressable>
            </View>
            {/* Conteneur d'envoie de donnée de géolocalisation */}
            <View style={styles.search}>
                <Text style={styles.title_geo}>Rechercher par Géolocalisation</Text>
                {/* Bouton qui envoie les données de la géolocalisation dans la requête de l'api */}
                {/* qui se situe sur la page ListPage */}
                <Pressable style={styles.btn_location}
                onPress={() =>
                    navigation.navigate('ListPage', { name: text })
                }
                >
                    <Text style={{color: '#fff'}}>Géolocaliser</Text>
                </Pressable>
            </View>
            
        </View>
    )
}

//styles de la page "HomePage"
const styles = StyleSheet.create ({
    container: {
        flex: 1,
        backgroundColor: '#4D4A95',
        color: '#fff',
        alignItems: 'center',
        paddingTop: 20,
    },
    search:{
        backgroundColor: '#fff',
        marginBottom: 30,
        padding: 10,
        borderRadius: 10,
    },
    btn_search:{
        backgroundColor: '#4D4A95',
        padding: 8,
        margin: 10,
        width: 300,
        borderRadius: 5,
        alignItems: 'center',
    },
    input: {
        padding: 8,
        margin: 10,
        width: 300,
        backgroundColor: "#fff",
        borderRadius: 5,
        borderBottomColor: '#000',
        borderBottomWidth: 1,
    },
    btn_location: {
        backgroundColor: '#4D4A95',
        padding: 8,
        margin: 10,
        width: 300,
        borderRadius: 5,
        alignItems: 'center',
    },
    title: {
        color: '#fff',
        fontSize: 32,
        fontWeight: '700',
        textAlign: 'center',
        marginBottom: 20,
    },
    title_geo:{
        padding: 8,
        margin: 10,
        width: 300,
        color: '#4D4A95',
        textAlign: 'center',
        fontSize: 18,
        fontWeight: '700'
    }
});