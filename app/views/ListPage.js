import React, { useEffect, useState } from 'react';
import { StyleSheet, ActivityIndicator, FlatList, Text, Image, TouchableWithoutFeedback, View, Dimensions } from 'react-native';
import { Items } from './Items';

const numColumns = 2;
export const ListPage = ({navigation, route}) => {

    //liste des constantes pour la requete de l'API
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);

    //fonction permettant de recupérer les données en format json provenant de la requête
    useEffect(() => {
        fetch('http://www.omdbapi.com/?s='+ route.params.name +'&apikey=68a85415&page=1')
          .then((response) => response.json())
          .then((responseJSON) => setData(responseJSON))
          .catch((error) => console.error(error))
          .finally(() => setLoading(false));
      }, []);

    return (
        //conteneur principale de la page
        <View style={styles.container}>
            {/* Titre de la page avec la saisie de la page précedente */}
            <Text style = {styles.title}>Resultat Pour : {route.params.name}</Text>
            {/* Liste des films, pour chaque item de la liste, la liste appelle la page Items */}
            {isLoading ? <ActivityIndicator/> : (
                <FlatList style = {styles.flatlist}
                    data={data['Search']}
                    renderItem={ ({ item }) => (
                        <Items navigate = {navigation} item = {item} />
                    )}
                    keyExtractor={(item, index) => index.toString()}
                    numColumns = {numColumns}
                />
            )}
        </View>
    )
}

//styles de la page
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        backgroundColor: '#4D4A95',
        paddingTop: 20,
    },
    flatlist: {
        color: '#fff',
        fontSize: 20,
        flex: 4,
        textAlign: 'center'
    },
    title: {
        color: '#fff',
        fontSize: 32,
        fontWeight: '700',
        textAlign: 'center',
        marginBottom: 20,
    },
    
});