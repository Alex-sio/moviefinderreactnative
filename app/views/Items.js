import { getCurrentPositionAsync } from 'expo-location';
import React, { useEffect, useState } from 'react';
import { StyleSheet, ActivityIndicator, FlatList, Text, Image, TouchableWithoutFeedback, View, Dimensions } from 'react-native';

//largeur de l'ecran du téléphone
const screenW = Dimensions.get('window').width;

//constante contenant une fonction qui permet la navigation de l'item à la page détail
export const Items = (props) => {
    const onPressItem = () => {
        props.navigate.navigate('Detail', { item: props.item.imdbID})
    }
    
    //affichage de l'item, il affiche le Poster du film
    return (
        <TouchableWithoutFeedback onPress={onPressItem} >
            <View style = {styles.container}>
                <Image style={styles.Image} source={{uri: props.item.Poster}} />
            </View>
        </TouchableWithoutFeedback>
    )
}

//styles de la page
const styles = StyleSheet.create ({
    container: {
        flex: 1,
        flexDirection: 'column',
        margin: 10,
        alignItems: 'center',
        backgroundColor: '#445565',
        height: screenW - 100,
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset:{
            width: 0,
            height: screenW - 100,
        },
        shadowOpacity: 1,
        shadowRadius: 20,
    },
    Text: {
        color: '#fff',
        fontWeight: '700',
        paddingTop: 10,
    },
    Image: {
        width: '100%',
        height: '100%',
        borderRadius: 10,
    }
});